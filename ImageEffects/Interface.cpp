#include "wx/wx.h"
#include "wx/frame.h"
#include "wx/string.h"
#include "Interface.h"
#include "Utils.h"

BEGIN_EVENT_TABLE(ImageWindow, wxPanel)
EVT_PAINT(ImageWindow::paintEvent)
EVT_SIZE(ImageWindow::OnSize)
END_EVENT_TABLE()

ImageWindow::ImageWindow(wxFrame* parent)
	: wxPanel(parent)
	, rawData(nullptr)
{
}

unsigned char* ImageWindow::getDataPtr()
{
	return rawData;
}

const unsigned char* const ImageWindow::getDataReadOnly() const
{
	return rawData;
}

bool ImageWindow::loadImage(const wxString& fileName)
{
	bool result = image.LoadFile(fileName, wxBITMAP_TYPE_ANY);
	rawData = image.GetData();
	return result;
}

void ImageWindow::saveImage()
{
	image.SaveFile(wxString("wx_copy_img.jpeg"));
}

int ImageWindow::getDataElementsNumber()
{
	// multiply by 3 because for each pixel we have 
	// data for R, G and B components
	return image.GetWidth()*image.GetHeight()*3;
}

int ImageWindow::getWidth()
{
	return image.GetWidth();
}

int ImageWindow::getHeight()
{
	return image.GetHeight();
}

void ImageWindow::paintEvent(wxPaintEvent & evt)
{
	wxPaintDC dc(this);
	render(dc);
}

void ImageWindow::render(wxDC&  dc)
{
	if (!rawData)
	{
		return;
	}

	int neww, newh;
	dc.GetSize(&neww, &newh);
	//
	//if (neww != w || newh != h)
	//{
	//	resized = wxBitmap(image.Scale(neww, newh , wxIMAGE_QUALITY_HIGH));
	//	w = neww;
	//	h = newh;
	//	dc.DrawBitmap(resized, 0, 0, false);
	//}
	//else{
	//	dc.DrawBitmap(resized, 0, 0, false);
	//}
	dc.DrawBitmap(wxBitmap(image.Scale(neww, newh, wxIMAGE_QUALITY_HIGH)), 0, 0);
}

void ImageWindow::OnSize(wxSizeEvent& event)
{
	Refresh();
	event.Skip();
}

ImageEffectsMainWindow::ImageEffectsMainWindow(const wxString& title, int windowWidth, int windowHeight)
	: wxFrame((wxWindow*)NULL, wxID_ANY, title)
	, width(windowWidth)
	, height(windowHeight)
{
	imageWindow = new ImageWindow(this);

	mainWindowSizer = new wxBoxSizer(wxHORIZONTAL);
	mainWindowSizer->Add(imageWindow, 1, wxEXPAND);
	SetSizer(mainWindowSizer);

	clHandler.init();

	SetClientSize(width, height);
	this->DragAcceptFiles(true);
}

ImageWindow* ImageEffectsMainWindow::getImageWindow()
{
	return imageWindow;
}

void ImageEffectsMainWindow::OnFileDrop(wxDropFilesEvent& ev)
{
	imageWindow->loadImage( ev.GetFiles()[0] );

	int dataSize = imageWindow->getDataElementsNumber();

	unsigned char* rawData = imageWindow->getDataPtr();

	clHandler.createOpenCLKernel(rawData, dataSize, imageWindow->getWidth(), imageWindow->getHeight());
	//clHandler.createBilateralFilterKernel(rawData, dataSize, imageWindow->getWidth(), imageWindow->getHeight());

	Refresh();
}

void ImageEffectsMainWindow::OnExit(wxCommandEvent& ev)
{

}

wxBEGIN_EVENT_TABLE(ImageEffectsMainWindow, wxFrame)
EVT_DROP_FILES(ImageEffectsMainWindow::OnFileDrop)
EVT_MENU(wxID_EXIT, ImageEffectsMainWindow::OnExit)
wxEND_EVENT_TABLE()

DECLARE_APP(ImageEffectsApp)
IMPLEMENT_APP_NO_MAIN(ImageEffectsApp)
IMPLEMENT_WX_THEME_SUPPORT
DECLARE_APP(ImageEffectsApp)