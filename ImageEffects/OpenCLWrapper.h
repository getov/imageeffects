#ifndef _OPENCL_WRAPPER_
#define _OPENCL_WRAPPER_

#include <CL/cl.h>
#include <vector>

class OpenCLWrapper
{
public:
	OpenCLWrapper();

	~OpenCLWrapper();

	void init();

	/// TODO: change this so it is more flexible and/or add support for multikernel
	void createOpenCLKernel(unsigned char* rawData, int size, int width, int height);

	/// tmp for test
	void createBilateralFilterKernel(unsigned char* rawData, int size, int width, int height);

private:
	/// OpenCL device ID
	cl_device_id deviceID;

	/// Array of platform IDs
	std::vector<cl_platform_id> platformID;

	cl_context context;
	cl_command_queue commandQueue;

	/// OpenCL compute program that may have multiple kernels
	cl_program program;

	cl_kernel kernel;

	cl_mem data;

	/// OpenCL error code
	cl_int errorCode;

	void initOpenCLPlatforms();

	void initOpenCLDevices();

	void createOpenCLContext();

	void createOpenCLQueue();

	void createAndBuildOpenCLProgram();
};

/// Check if there is an OpenCL error, print the error code and exit the program
/// Called in CHECK_CL_ERROR()
template <typename T>
inline void _checkOpenCLError(const char* file, int line, T error)
{
#ifdef OPENCL_DEBUG
	if (error != CL_SUCCESS)
	{
		printf("error %i in file %s, line %i\n", error, file, line);
		exit(EXIT_FAILURE);
	}
#endif
}

/// Check if there is an OpenCL error, print the error code and exit the program
/// Internally calls _checkOpenCLError()
template <typename T>
inline void CHECK_CL_ERROR(T error)
{
	_checkOpenCLError(__FILE__, __LINE__, error);
}

#endif