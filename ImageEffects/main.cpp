#include "wx/wx.h"
#include "Interface.h"

#include <thread>
#include <string>
#include <CL/cl.h>
#include "Utils.h"
#include "OpenCLWrapper.h"

bool ImageEffectsApp::OnInit()
{
	wxInitAllImageHandlers();

	mainWindow = new ImageEffectsMainWindow(_T("ImageEffects"), 800, 600);

	SetTopWindow(mainWindow);
	mainWindow->Show(true);
	return true;
}

int ImageEffectsApp::OnExit()
{
	return 0;
}

int main(int argc, char* argv[])
{
	int display = true;

	int result = 0;
#ifdef _WIN32
	if (display)
	{
		result = wxEntry(argc, argv);
	}
#else
	if (display) 
	{
#ifndef __APPLE__
		// Linux-specific: if we were started with no arguments from a non-GUI console,
		// proceeding further will fail with "Error: failed to open display """ or even a crash.
		// So we detect this case and print a more useful message instead:
		const char* display = getenv("DISPLAY");
		bool gui = true;
		if (!display || !display[0]) 
		{
			gui = false;
		}
		if (!gui && argc <= 1) 
		{
			return 1;
		}
#endif
		result = wxEntry(argc, argv);
	}
#endif

	return result;
}