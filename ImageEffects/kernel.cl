int iclamp(int x, int a, int b)
{
    return max(a, min(b, x));
}

// convert floating point rgb color to 8-bit integer
uint rgbToInt(float r, float g, float b)
{
  r = clamp(r, 0.0f, 255.0f);
  g = clamp(g, 0.0f, 255.0f);
  b = clamp(b, 0.0f, 255.0f);
  return (convert_uint(b)<<16) + (convert_uint(g)<<8) + convert_uint(r);
}

// get pixel from 2D image, with clamping to border
uchar getPixel(__global uchar* data, int x, int y, int width, int height)
{
    x = iclamp(x, 0, width - 1);
    y = iclamp(y, 0, height - 1);
    return data[y * width + x];
}

int nearestInt(float x)
{
	return (int)(floor(x + 0.5f));
}

unsigned _convertTo8bit(float x)
{
	if (x <= 0) return 0;
	if (x >= 1) return 255;
	return nearestInt(x * 255.0f);
}

// calculate FStops value parameter from the arguments
float resetFStopsParameter( float powKLow, float kHigh )
{
    float curveBoxWidth = pow( 2.0f, kHigh ) - powKLow;
    float curveBoxHeight = pow( 2.0f, 3.5f )  - powKLow;

    // Initial boundary values
    float fFStopsLow = 0.0f;
    float fFStopsHigh = 100.0f;
    int iterations = 23; //interval bisection iterations

    // Interval bisection to find the final knee function fStops parameter
    for ( int i = 0; i < iterations; i++ )
    {
        float fFStopsMiddle = ( fFStopsLow + fFStopsHigh ) * 0.5f;
        if ( ( curveBoxWidth * fFStopsMiddle + 1.0f ) < exp( curveBoxHeight * fFStopsMiddle ) )
        {
            fFStopsHigh = fFStopsMiddle;
        }
        else
        {
            fFStopsLow = fFStopsMiddle;
        }
    }

    return ( fFStopsLow + fFStopsHigh ) * 0.5f;
}

typedef float4 Color;

 __kernel void processImage(__global uchar* restrict data, const int count, int width, int height)
{
    int x = get_global_id(0);
    int y = get_global_id(1);

	int rowOffset = y * width * 3;
	int index = 3 * x + rowOffset;
	
	if( x >= width || y >= height ) return;

	Color pixel;
	pixel.x = data[index] / 255.0f;
	pixel.y = data[index+1] / 255.0f;
	pixel.z = data[index+2] / 255.0f;
	pixel.w = 0.0f;

	float grayscale = pixel.x * 0.299f + pixel.y * 0.587f + pixel.z * 0.114f;

	data[index] = _convertTo8bit(grayscale);
	data[index+1] = _convertTo8bit(grayscale);
	data[index+2] = _convertTo8bit(grayscale);
}


typedef struct
{
    float fPowKLow;                             // fPowKLow = pow( 2.0f, kLow)
    float fPowKHigh;                            // fPowKHigh = pow( 2.0f, kHigh)
    float fPow35;                               // fPow35 = pow( 2.0f, 3.5f)
    float fFStops;                              // F stops
    float fFStopsInv;                           // Invesrse fFStops value
    float fPowExposure;                         // fPowExposure = pow( 2.0f, exposure +  2.47393f )
    float fGamma;                               // Gamma correction parameter
    float fPowGamma;                            // Scale factor
    float fDefog;                               // Defog value
} CHDRData;

 __kernel void toneMapping(__global uchar* restrict data, const int count, int width, int height, CHDRData HDRData)
{
    int x = get_global_id(0);
    int y = get_global_id(1);

	int rowOffset = y * width * 3;
	int index = 3 * x + rowOffset;
	
	if( x >= width || y >= height ) return;

	Color fColor;
	fColor.x = data[index] / 255.0f;
	fColor.y = data[index+1] / 255.0f;
	fColor.z = data[index+2] / 255.0f;
	fColor.w = 0.0f;

	// Load tone mapping parameters
    float4 fPowKLow = (float4)HDRData.fPowKLow;
    float4 fFStops = (float4)HDRData.fFStops;
    float4 fFStopsInv = (float4)HDRData.fFStopsInv;
    float4 fPowExposure = (float4)HDRData.fPowExposure;
    float4 fDefog = (float4)HDRData.fDefog;
    float4 fGamma = (float4)HDRData.fGamma;
    float4 fPowGamma = (float4)HDRData.fPowGamma;

	// and define method constants.
    float4 fOne = 1.0f;
    float4 fZerro = 0.0f;
    float4 fSaturate = 255.f;

    // Defog
    fColor = fColor - fDefog;
    fColor = max(fZerro, fColor);

    // Multiply color by pow( 2.0f, exposure +  2.47393f )
    fColor = fColor * fPowExposure;

    int4 iCmpFlag = 0;
    // iCmpFlag = isgreater(fColor, fPowKLow);
    iCmpFlag = fColor > fPowKLow;

    if(any(iCmpFlag))
    {
        // fPowKLow = 2^kLow
        // fFStopsInv = 1/fFStops;
        // fTmpPixel = fPowKLow + log((fTmpPixel-fPowKLow) * fFStops + 1.0f)*fFStopsInv;
        float4 fTmpPixel = fColor - fPowKLow;
        fTmpPixel = fTmpPixel * fFStops;
        fTmpPixel = fTmpPixel + fOne;
        fTmpPixel = native_log( fTmpPixel );
        fTmpPixel = fTmpPixel * fFStopsInv;
        fTmpPixel = fTmpPixel + fPowKLow;

        // color channels update versions
        ///fColor = select(fTmpPixel, fColor, iCmpFlag);
        ///fColor = select(fColor, fTmpPixel, iCmpFlag);
        fColor = fTmpPixel;
    }

    // Gamma correction
    fColor = powr(fColor, fGamma);

    // Scale the values
    fColor = fColor * fSaturate;
    fColor = fColor * fPowGamma;

    // Saturate
    fColor = max(fColor, 0.f);
    fColor = min(fColor, fSaturate);

    // Store result pixel
	data[index]   = _convertTo8bit(fColor.x);
	data[index+1] = _convertTo8bit(fColor.y);
	data[index+2] = _convertTo8bit(fColor.z);
}

#define POW2(a) ((a) * (a))
__kernel void bilateral_filter(__global uchar* restrict data, int width, int height)
{
	float radius = 2.0f;
	float preserve = 1.0f;

    int x = get_global_id(0);
    int y = get_global_id(1);

	if( x >= width || y >= height ) return;

	int gidx = x*3;
	int gidy = y*3;

    int n_radius   = ceil(radius);
    int dst_width  = get_global_size(0);
    int src_width  = dst_width + n_radius * 2;

	Color fColor;
	fColor.x = data[(gidy + n_radius) * src_width + gidx + n_radius] / 255.0f;
	fColor.y = data[(gidy + n_radius) * src_width + gidx + n_radius+1] / 255.0f;
	fColor.z = data[(gidy + n_radius) * src_width + gidx + n_radius+2] / 255.0f;
	fColor.w = 0.0f;

    int u, v, i, j;
    float4 center_pix = fColor;// in[(gidy + n_radius) * src_width + gidx + n_radius];
    float4 accumulated = 0.0f;
    float4 tempf       = 0.0f;
    float  count       = 0.0f;
    float  diff_map, gaussian_weight, weight;
	
    for (v = -n_radius;v <= n_radius; ++v)
    {
        for (u = -n_radius;u <= n_radius; ++u)
        {
            i = gidx + n_radius + u;
            j = gidy + n_radius + v;
	
            int gid1d = i + j * src_width;
			Color color2;
			color2.x = data[gid1d] / 255.0f;
			color2.y = data[gid1d+1] / 255.0f;
			color2.z = data[gid1d+2] / 255.0f;
			color2.w = 0.0f;
            tempf = color2;//in[gid1d];
	
            diff_map = exp (
                - (   POW2(center_pix.x - tempf.x)
                    + POW2(center_pix.y - tempf.y)
                    + POW2(center_pix.z - tempf.z))
                * preserve);
	
            gaussian_weight =
                exp( - 0.5f * (POW2(u) + POW2(v)) / radius);
	
            weight = diff_map * gaussian_weight;
	
            accumulated += tempf * weight;
            count += weight;
        }
    }
	Color result = accumulated / count;
    ////out[gidx + gidy * dst_width] = accumulated / count;
	data[gidx + gidy * dst_width] = _convertTo8bit(result.x);
	data[gidx + gidy * dst_width + 1] = _convertTo8bit(result.y);
	data[gidx + gidy * dst_width + 2] = _convertTo8bit(result.z);
}