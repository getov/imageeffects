#ifndef _INTERFACE_H_
#define _INTERFACE_H_

#include "OpenCLWrapper.h"

// forward declarations just in case
class wxApp;
class wxFrame;
class wxString;
class wxCommandEvent;

class ImageWindow : public wxPanel
{
private:
	wxImage image;
	wxBitmap resized;
	
	/// The raw image data
	/// This is pointer to the image data in
	/// the following format: RGBRGB...RGB
	/// Each value is 8bit in the range of [0, 255]
	unsigned char* rawData;

public:
	ImageWindow(wxFrame* parent);

	/// Return a pointer to the raw image data
	/// The data is in the following format: RGBRGB...RGB
	/// Each value is 8bit in the range of [0, 255]
	/// Use this only for direct access of the data and when need to edit it.
	unsigned char* getDataPtr();

	/// Return a pointer to the raw image data
	/// The data is in the following format: RGBRGB...RGB
	/// Each value is 8bit in the range of [0, 255]
	/// Use this for read only
	const unsigned char* const getDataReadOnly() const;

	/// Returns the number of data elements we have for the image
	/// Returns image width*height*3, because we have to account for the R, G and B components
	int getDataElementsNumber();

	/// Get image width
	int getWidth();

	/// Get image height
	int getHeight();

	/// Loads image file.
	/// Returns true if the operation is successful and false otherwise
	/// Internally calls wxImage::loadFile() and on error wx should
	/// handle it and call wxLogError()
	bool loadImage(const wxString& fileName);

	/// Saves the image
	void saveImage();

	/// Called by the system of by wxWidgets when the ImageWindow needs
	/// to be redrawn. Can also be triggered by Refresh() / Update()
	void paintEvent(wxPaintEvent & evt);

	/// Resize the image
	void OnSize(wxSizeEvent& event);
	
	/// The actual rendering
	void render(wxDC& dc);

	DECLARE_EVENT_TABLE()
};

class ImageEffectsMainWindow : public wxFrame
{
public:
	ImageEffectsMainWindow(const wxString& title, int windowWidth, int windowHeight);

	/// Return pointer to ImageWindow
	ImageWindow* getImageWindow();

private:
	int width;
	int height;

	/// The window that contain information for the loaded image
	ImageWindow* imageWindow;

	/// Sizer element for the main window that handles the interface
	wxBoxSizer* mainWindowSizer;

	/// Handler for calling custum wrapped OpenCL functions
	OpenCLWrapper clHandler;

	void OnFileDrop(wxDropFilesEvent& ev);
	void OnExit(wxCommandEvent& ev);

	wxDECLARE_EVENT_TABLE();
};

class ImageEffectsApp : public wxApp
{
public:
	virtual bool OnInit();
	virtual int OnExit();
private:
	ImageEffectsMainWindow* mainWindow;
};



#endif