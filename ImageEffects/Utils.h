#ifndef _UTILS_H_
#define _UTILS_H_

/// Custom implementation of true and false type
template <typename A, A val>
struct integralConstant
{
	static const A value = val;

	typedef A valueType;

	operator valueType() const
	{
		return value;
	}
};

typedef integralConstant<bool, true> trueType;
typedef integralConstant<bool, false> falseType;

template <typename T, typename U>
struct isSame
	: falseType
{
};

template <typename T>
struct isSame<T, T>
	: trueType
{
};

struct Color
{
	float r, g, b;
};

inline float intensityPerceptual(const Color& color)
{
	return (color.r * 0.299 + color.g * 0.587 + color.b * 0.114);
}

inline int nearestInt(float x)
{
	return static_cast<int>(floor(x + 0.5f));
}

const bool sRGBon = false;
inline unsigned _convertTo8bit(float x, trueType)
{
	const float a = 0.055f;
	if (x <= 0) return 0;
	if (x >= 1) return 255;
	// sRGB transform:
	if (x <= 0.0031308f)
		x = x * 12.02f;
	else
		x = (1.0f + a) * powf(x, 1.0f / 2.4f) - a;
	return nearestInt(x * 255.0f);
}

inline unsigned _convertTo8bit(float x, falseType)
{
	const float a = 0.055f;
	if (x <= 0) return 0;
	if (x >= 1) return 255;
	return nearestInt(x * 255.0f);
}

inline unsigned convertTo8bit(float x)
{
	return _convertTo8bit(x, integralConstant<bool, sRGBon>());
}

inline size_t shrRoundUp(size_t localWorkSize, size_t numItems) 
{
	size_t result = localWorkSize;
	while (result < numItems)
	{
		result += localWorkSize;
	}

	return result;
}

typedef struct
{
	float fPowKLow;                             //fPowKLow = pow( 2.0f, kLow)
	float fPowKHigh;                            //fPowKHigh = pow( 2.0f, kHigh)
	float fPow35;                               //fPow35 = pow( 2.0f, 3.5f)
	float fFStops;                              //F stops
	float fFStopsInv;                           //Invesrse fFStops value
	float fPowExposure;                         //fPowExposure = pow( 2.0f, exposure +  2.47393f )
	float fGamma;                               //Gamma correction parameter
	float fPowGamma;                            //Scale factor
	float fDefog;                               //Defog value
} CHDRData;

inline float resetFStopsParameter(float powKLow, float kHigh)
{
	float curveBoxWidth = pow(2.0f, kHigh) - powKLow;
	float curveBoxHeight = pow(2.0f, 3.5f) - powKLow;

	// Initial boundary values
	float fFStopsLow = 0.0f;
	float fFStopsHigh = 100.0f;
	int iterations = 23; //interval bisection iterations

	// Interval bisection to find the final knee function fStops parameter
	for (int i = 0; i < iterations; i++)
	{
		float fFStopsMiddle = (fFStopsLow + fFStopsHigh) * 0.5f;
		if ((curveBoxWidth * fFStopsMiddle + 1.0f) < exp(curveBoxHeight * fFStopsMiddle))
		{
			fFStopsHigh = fFStopsMiddle;
		}
		else
		{
			fFStopsLow = fFStopsMiddle;
		}
	}

	return (fFStopsLow + fFStopsHigh) * 0.5f;
}

#endif