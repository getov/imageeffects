#include "OpenCLWrapper.h"
#include "Utils.h"
#include <fstream>
#include <streambuf>

OpenCLWrapper::OpenCLWrapper()
	: deviceID(nullptr)
	, context(nullptr)
	, commandQueue(nullptr)
	, program(nullptr)
	, kernel(nullptr)
	, data(nullptr)
	, errorCode(CL_SUCCESS)
{
}

void OpenCLWrapper::initOpenCLPlatforms()
{
	cl_uint numPlatforms = 0;

	//when called with second arg NULL, clGetPlatformIDs returns in its
	//third arg the number of the platforms the system has
	errorCode = clGetPlatformIDs(0, NULL, &numPlatforms);
	CHECK_CL_ERROR(errorCode);
	printf("Found %i platforms\n", (int) numPlatforms);

	if (numPlatforms == 0)
		exit(EXIT_FAILURE);

	//now, alloc one unique id per platform
	platformID.resize(numPlatforms);

	//and get the ids of all platforms the system has
	errorCode = clGetPlatformIDs(numPlatforms, &platformID[0], NULL);

	//print the name of each and every platfrom we had found
	for (int i = 0; i < numPlatforms; ++i) 
	{
		char chBuffer[1024];
		errorCode = clGetPlatformInfo(platformID[i], CL_PLATFORM_NAME, 1024, &chBuffer, NULL);
		CHECK_CL_ERROR(errorCode);
		printf("found platform '%i' = \"%s\"\n", i, chBuffer);
	}
}

void OpenCLWrapper::initOpenCLDevices()
{
	errorCode = clGetDeviceIDs(platformID[0], CL_DEVICE_TYPE_GPU | CL_DEVICE_TYPE_CPU, 1, &deviceID, NULL);
	CHECK_CL_ERROR(errorCode);
}

void OpenCLWrapper::createOpenCLContext()
{
	context = clCreateContext(0 /*platform id 0*/, 1 /*one device from it*/, &deviceID, NULL, NULL, &errorCode);
	CHECK_CL_ERROR(errorCode);
}

void OpenCLWrapper::createOpenCLQueue()
{
	commandQueue = clCreateCommandQueue(context, deviceID, 0, &errorCode);
	CHECK_CL_ERROR(errorCode);
}

void OpenCLWrapper::createAndBuildOpenCLProgram()
{
	std::ifstream t("..//..//ImageEffects//kernel.cl");
	std::string str((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());
	const char* file = str.c_str();
	program = clCreateProgramWithSource(context, 1, (const char**)&file, NULL, &errorCode);
	CHECK_CL_ERROR(errorCode);

	//compile the program
	errorCode = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
	if (errorCode != CL_SUCCESS) 
	{
		size_t len;
		char buffer[2048];
		printf("Error: Failed to build program executable!\n");
		clGetProgramBuildInfo(program, deviceID, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
		printf("%s\n", buffer);
		exit(EXIT_FAILURE);
	}
}

void OpenCLWrapper::init()
{
	initOpenCLPlatforms();
	initOpenCLDevices();
	createOpenCLContext();
	createOpenCLQueue();
	createAndBuildOpenCLProgram();
}

void OpenCLWrapper::createOpenCLKernel(unsigned char* rawData, int size, int width, int height)
{
	kernel = clCreateKernel(program, "toneMapping", &errorCode);
	CHECK_CL_ERROR(errorCode);

	//Create the input and output arrays in device memory for our calculation
	//cl_mem input;  // handle to device memory used for the input array
	//cl_mem data; // handle to device memory used for the output array

	//(4) allocate memory on the device
	//memory marked as input/output can increase the performance of the kernel
	data = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(unsigned char) * size, NULL, &errorCode);
	CHECK_CL_ERROR(errorCode);

	//Copy data to the device memory
	errorCode = clEnqueueWriteBuffer(commandQueue, data, CL_TRUE, 0, sizeof(unsigned char) * size, rawData, 0, NULL, NULL);
	CHECK_CL_ERROR(errorCode);

	///------------------------------
	// init HDR parameters
	float kLow = -3.0f;
	float kHigh = 7.5f;
	float exposure = 3.0f;
	float gamma = 1.0f;
	float defog = 0.0f;
	// fill HDR parameters structure
	CHDRData HDRData;
	HDRData.fGamma = gamma;
	HDRData.fPowGamma = pow(2.0f, -3.5f*gamma);
	HDRData.fDefog = defog;
	HDRData.fPowKLow = pow(2.0f, kLow);
	HDRData.fPowKHigh = pow(2.0f, kHigh);
	HDRData.fPow35 = pow(2.0f, 3.5f);
	HDRData.fPowExposure = pow(2.0f, exposure + 2.47393f);
	// calculate FStops
	HDRData.fFStops = resetFStopsParameter(HDRData.fPowKLow, kHigh);
	HDRData.fFStopsInv = 1.0f / HDRData.fFStops;
	///------------------------------

	//Prepare to call the kernel
	//Set the arguments to our compute kernel
	errorCode = 0;
	errorCode |= clSetKernelArg(kernel, 0, sizeof(cl_mem), &data);
	errorCode |= clSetKernelArg(kernel, 1, sizeof(int), &size);
	errorCode |= clSetKernelArg(kernel, 2, sizeof(int), &width);
	errorCode |= clSetKernelArg(kernel, 3, sizeof(int), &height);
	errorCode |= clSetKernelArg(kernel, 4, sizeof(CHDRData), &HDRData);
	CHECK_CL_ERROR(errorCode);

	size_t global[2];  // number of thread blocks
	size_t local[2];   // thread block size

	//Get the maximum work group size for executing the kernel on the device
	//err = clGetKernelWorkGroupInfo(kernel, device_id, CL_KERNEL_WORK_GROUP_SIZE, sizeof(local), &local, NULL);
	//CHECK_ERROR(err);

	//(5) Execute the kernel over the entire range of our 1d input data set
	//using the maximum number of work group items for this device

	local[0] = 32;
	local[1] = 32;

	global[0] = shrRoundUp(local[0], width * 3);
	global[1] = shrRoundUp(local[1], height * 3);

	errorCode = clEnqueueNDRangeKernel(commandQueue, kernel, 2, NULL, global, local, 0, NULL, NULL);
	CHECK_CL_ERROR(errorCode);

	//Wait for the command commands to get serviced before reading back results
	clFinish(commandQueue);

	//(6) Read back the results from the device to verify the output
	errorCode = clEnqueueReadBuffer(commandQueue, data, CL_TRUE, 0, sizeof(unsigned char) * size, rawData, 0, NULL, NULL);
	CHECK_CL_ERROR(errorCode);

	clReleaseMemObject(data);
	clReleaseKernel(kernel);
}

void OpenCLWrapper::createBilateralFilterKernel(unsigned char* rawData, int size, int width, int height)
{
	kernel = clCreateKernel(program, "bilateral_filter", &errorCode);
	CHECK_CL_ERROR(errorCode);

	//Create the input and output arrays in device memory for our calculation
	//cl_mem input;  // handle to device memory used for the input array
	//cl_mem data; // handle to device memory used for the output array

	//(4) allocate memory on the device
	//memory marked as input/output can increase the performance of the kernel
	data = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(unsigned char) * size, NULL, &errorCode);
	CHECK_CL_ERROR(errorCode);

	//Copy data to the device memory
	errorCode = clEnqueueWriteBuffer(commandQueue, data, CL_TRUE, 0, sizeof(unsigned char) * size, rawData, 0, NULL, NULL);
	CHECK_CL_ERROR(errorCode);

	//Prepare to call the kernel
	//Set the arguments to our compute kernel
	errorCode = 0;
	errorCode |= clSetKernelArg(kernel, 0, sizeof(cl_mem), &data);
	errorCode |= clSetKernelArg(kernel, 1, sizeof(int), &width);
	errorCode |= clSetKernelArg(kernel, 2, sizeof(int), &height);
	CHECK_CL_ERROR(errorCode);

	size_t global[2];  // number of thread blocks
	size_t local[2];   // thread block size

	local[0] = 32;
	local[1] = 32;

	global[0] = shrRoundUp(local[0], width * 3);
	global[1] = shrRoundUp(local[1], height * 3);

	errorCode = clEnqueueNDRangeKernel(commandQueue, kernel, 2, NULL, global, local, 0, NULL, NULL);
	CHECK_CL_ERROR(errorCode);

	//Wait for the command commands to get serviced before reading back results
	clFinish(commandQueue);

	//(6) Read back the results from the device to verify the output
	errorCode = clEnqueueReadBuffer(commandQueue, data, CL_TRUE, 0, sizeof(unsigned char) * size, rawData, 0, NULL, NULL);
	CHECK_CL_ERROR(errorCode);

	clReleaseMemObject(data);
	clReleaseKernel(kernel);
}

OpenCLWrapper::~OpenCLWrapper()
{
	clReleaseProgram(program);
	clReleaseCommandQueue(commandQueue);
	clReleaseContext(context);
}